﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrainTimer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BrainTimer))
        Me.ButtonStudy = New System.Windows.Forms.Button()
        Me.ButtonBreak = New System.Windows.Forms.Button()
        Me.TextBoxStudy = New System.Windows.Forms.TextBox()
        Me.TextBoxBreak = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TimerMain = New System.Windows.Forms.Timer(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeveloperToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeveloperToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonStudy
        '
        Me.ButtonStudy.Location = New System.Drawing.Point(24, 75)
        Me.ButtonStudy.Name = "ButtonStudy"
        Me.ButtonStudy.Size = New System.Drawing.Size(75, 65)
        Me.ButtonStudy.TabIndex = 0
        Me.ButtonStudy.UseVisualStyleBackColor = True
        '
        'ButtonBreak
        '
        Me.ButtonBreak.Location = New System.Drawing.Point(105, 75)
        Me.ButtonBreak.Name = "ButtonBreak"
        Me.ButtonBreak.Size = New System.Drawing.Size(75, 65)
        Me.ButtonBreak.TabIndex = 1
        Me.ButtonBreak.UseVisualStyleBackColor = True
        '
        'TextBoxStudy
        '
        Me.TextBoxStudy.Location = New System.Drawing.Point(24, 49)
        Me.TextBoxStudy.Name = "TextBoxStudy"
        Me.TextBoxStudy.Size = New System.Drawing.Size(75, 20)
        Me.TextBoxStudy.TabIndex = 2
        Me.TextBoxStudy.Text = "10"
        '
        'TextBoxBreak
        '
        Me.TextBoxBreak.Location = New System.Drawing.Point(105, 49)
        Me.TextBoxBreak.Name = "TextBoxBreak"
        Me.TextBoxBreak.Size = New System.Drawing.Size(75, 20)
        Me.TextBoxBreak.TabIndex = 3
        Me.TextBoxBreak.Text = "5"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Study Minutes"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(106, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Break Minutes"
        '
        'TimerMain
        '
        Me.TimerMain.Enabled = True
        Me.TimerMain.Interval = 1000
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(203, 24)
        Me.MenuStrip1.TabIndex = 6
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeveloperToolStripMenuItem1, Me.DeveloperToolStripMenuItem})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'DeveloperToolStripMenuItem1
        '
        Me.DeveloperToolStripMenuItem1.Name = "DeveloperToolStripMenuItem1"
        Me.DeveloperToolStripMenuItem1.Size = New System.Drawing.Size(127, 22)
        Me.DeveloperToolStripMenuItem1.Text = "Use"
        '
        'DeveloperToolStripMenuItem
        '
        Me.DeveloperToolStripMenuItem.Name = "DeveloperToolStripMenuItem"
        Me.DeveloperToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.DeveloperToolStripMenuItem.Text = "Developer"
        '
        'BrainTimer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(203, 147)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxBreak)
        Me.Controls.Add(Me.TextBoxStudy)
        Me.Controls.Add(Me.ButtonBreak)
        Me.Controls.Add(Me.ButtonStudy)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "BrainTimer"
        Me.Text = "BrainTimer"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonStudy As System.Windows.Forms.Button
    Friend WithEvents ButtonBreak As System.Windows.Forms.Button
    Friend WithEvents TextBoxStudy As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxBreak As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TimerMain As System.Windows.Forms.Timer
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeveloperToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeveloperToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
