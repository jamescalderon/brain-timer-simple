﻿Imports Microsoft.VisualBasic
Imports System.Timers
Public Class BrainTimer
    Dim eventIn As String
    Dim timeRemainingStudy As Integer
    Dim timeRemainingBreak As Integer




    Private Sub BrainTimer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'load event
        TimerMain.Stop() 'prevents timer from
        'set initial values
        eventIn = "study" 'assume default action is study
        ButtonStudy.Text = TextBoxStudy.Text
        ButtonBreak.Text = TextBoxBreak.Text

        'convert text to minutes
        timeRemainingStudy = Integer.Parse(TextBoxStudy.Text) * 60
        timeRemainingBreak = Integer.Parse(TextBoxBreak.Text) * 60


    End Sub

    Private Sub ButtonStudy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonStudy.Click
        eventIn = "study"
        timeRemainingStudy = Integer.Parse(TextBoxStudy.Text) * 60
        ButtonStudy.ForeColor = Color.Green
        TimerMain.Start()
    End Sub
    Private Sub ButtonBreak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBreak.Click
        eventIn = "break"
        timeRemainingBreak = Integer.Parse(TextBoxBreak.Text) * 60
        ButtonBreak.ForeColor = Color.Red
        TimerMain.Start()
    End Sub
    Private Sub TextBoxStudy_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxStudy.TextChanged
        Try
            timeRemainingStudy = Integer.Parse(TextBoxStudy.Text)
            ButtonStudy.Enabled = True
            ButtonStudy.Text = TextBoxStudy.Text
        Catch ex As Exception
            ButtonStudy.Text = "WHOLE MINUTES ONLY!"
            ButtonStudy.Enabled = False
        End Try
    End Sub

    Private Sub TextBoxBreak_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxBreak.TextChanged
        Try
            timeRemainingBreak = Integer.Parse(TextBoxBreak.Text)
            ButtonBreak.Enabled = True
            ButtonBreak.Text = TextBoxBreak.Text
        Catch ex As Exception
            ButtonBreak.Text = "WHOLE MINUTES ONLY!"
            ButtonBreak.Enabled = False
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMain.Tick
        

        Select Case eventIn
            Case "study"
                timeRemainingStudy -= 1 'deduct 1 sec

                If timeRemainingStudy = 0 Then
                    TimerMain.Stop()
                    ButtonStudy.ForeColor = Form.DefaultForeColor
                    System.Media.SystemSounds.Exclamation.Play()

                    MsgBox("STUDY TIME UP!")


                End If

            Case "break"
                timeRemainingBreak -= 1 'deduct 1 sec

                If timeRemainingBreak = 0 Then
                    TimerMain.Stop()
                    ButtonBreak.ForeColor = Form.DefaultForeColor
                    System.Media.SystemSounds.Exclamation.Play()
                    MsgBox("BREAK TIME UP!")


                End If
        End Select
    End Sub

   
    
    Private Sub DeveloperToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeveloperToolStripMenuItem1.Click
        MsgBox("Just type in minutes for study/breaks to maximize learning! If you want to asign a sound to the popup, change the Exclamation sound in Control Panel>Sounds>Sound Tab>Windows Exclamation.")
    End Sub

    Private Sub DeveloperToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeveloperToolStripMenuItem.Click
        MsgBox("Developer:" & "     " & "James Calderon" & vbCrLf & "Site:                jamescalderon.com", MsgBoxStyle.Information, "About Information")
    End Sub
End Class
